import { MatPaginator } from "@angular/material/paginator";

export const configurePaginator = (paginator: MatPaginator): MatPaginator => {
    paginator._intl.firstPageLabel = 'Primera página';
    paginator._intl.itemsPerPageLabel = 'Resultados por página';
    paginator._intl.lastPageLabel = 'Última página';
    paginator._intl.nextPageLabel = 'Página siguiente';
    paginator._intl.previousPageLabel = 'Página anterior';
    paginator._intl.getRangeLabel = getRangeLabel;

    return paginator;
};

const getRangeLabel = (page: number, pageSize: number, length: number): string => {

    if (length === 0 || pageSize === 0) {
        return `de ${length}`;
    }

    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex} de ${length}`;
}
