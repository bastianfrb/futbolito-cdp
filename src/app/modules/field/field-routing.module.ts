import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainFieldComponent } from './components/main-field/main-field.component';

const routes: Routes = [
  {
    path: '',
    component: MainFieldComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FieldRoutingModule { }
