export interface IField {
    id: number;
    location: string;
    name: string;
    price: number;
    options: boolean;
}
