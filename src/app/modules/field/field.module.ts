import { NgModule } from '@angular/core';
import { CreateFieldComponent } from './components/create-field/create-field.component';
import { MainFieldComponent } from './components/main-field/main-field.component';
import { ModalEditFieldComponent } from './components/modal-edit-field/modal-edit-field.component';
import { FieldRoutingModule } from './field-routing.module';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { FieldFormComponent } from './components/field-form/field-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { FieldTableComponent } from './components/field-table/field-table.component';
@NgModule({
  declarations: [
    CreateFieldComponent,
    MainFieldComponent,
    ModalEditFieldComponent,
    FieldFormComponent,
    FieldTableComponent,
  ],
  imports: [
    FieldRoutingModule,
    MatCardModule,
    MatExpansionModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatDialogModule
  ],
})
export class FieldModule { }
