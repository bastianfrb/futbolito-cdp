import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CacheService } from '../../shared/services/cache/cache.service';
import { IField } from '../interfaces/field';

@Injectable({
  providedIn: 'root'
})
export class FieldService {

  public fieldChanges: Subject<IField[]>;

  constructor(private cache: CacheService) { 
    this.fieldChanges = new Subject<IField[]>();
  }

  /**
   * Function to get maximun ID (provisory)
   * @returns max ID
   */
  public getMaxId(): number {
    const fields = this.getFields();
    let maxId = 0;

    for (const field of fields) {
      maxId = Number(field.id) > maxId ? field.id : maxId;
    }

    return maxId;
  }

  /**
   * Function to get available fields
   */
  public getFields(): IField[] {
    return this.cache.local('fields') as IField[] || [];
  }

  /**
   * Function to get a field by id
   * @id Field id
   */
  public getFieldById(id: number): IField {
    const field = this.getFields().find((v) => v.id === id) as IField;
    return field;
  }
  
  /**
   * Function to save one new field
   * @field new field
   */
  public saveField(field: IField): void {
    let fields = this.cache.local('fields') as IField[];
    if (!fields) {
      fields = [];
    }

    fields.push(field);

    this.cache.saveLocal('fields', fields);
    this.fieldChanges.next(fields);
  }
  
  /**
   * Function to edit a field
   * @param field Field to edit
   */
  public editField(field: IField): void {
    const fields = this.getFields();
    const foundField = fields.find((v) => v.id === field.id) as IField;

    foundField.location = field.location;
    foundField.name = field.name;
    foundField.price = field.price;

    this.cache.saveLocal('fields', fields);
    this.fieldChanges.next(fields);
  }

  /**
   * Function to remove a field
   * @param id Field id to remove
   */
  public removeField(id: number): void {
    const fields = this.getFields().filter((v) => v.id !== id);

    this.cache.saveLocal('fields', fields);
    this.fieldChanges.next(fields);
  }
}
