import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEditFieldComponent } from './modal-edit-field.component';

describe('ModalEditFieldComponent', () => {
  let component: ModalEditFieldComponent;
  let fixture: ComponentFixture<ModalEditFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalEditFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
