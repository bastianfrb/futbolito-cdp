import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IField } from '../../interfaces/field';

@Component({
  selector: 'app-modal-edit-field',
  templateUrl: './modal-edit-field.component.html',
  styleUrls: ['./modal-edit-field.component.scss']
})
export class ModalEditFieldComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ModalEditFieldComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IField,
  ) { }

  ngOnInit(): void {
  }

  public closeDialog(field: IField): void {
    this.dialogRef.close(field ? field : this.data);
  }
}
