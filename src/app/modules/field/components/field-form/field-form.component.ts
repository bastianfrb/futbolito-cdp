import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IField } from '../../interfaces/field';
import { FieldService } from '../../services/field.service';

@Component({
  selector: 'app-field-form',
  templateUrl: './field-form.component.html',
  styleUrls: ['./field-form.component.scss']
})
export class FieldFormComponent implements OnInit {

  @Input() public fieldData?: IField;

  @Output() public fieldChange: EventEmitter<IField>;

  public formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private fieldService: FieldService
  ) {
    this.fieldChange = new EventEmitter<IField>();

    this.formGroup = this.fb.group({
      location: [''],
      name: [''],
      price: ['']
    });
  }

  ngOnInit(): void {
    if (this.fieldData) {
      this.formGroup.patchValue({
        location: this.fieldData.location,
        name: this.fieldData.name,
        price: this.fieldData.price
      });
    }
  }

  /**
   * Function to send form outside
   */
  public submitForm(): void {
    const f = this.formGroup.controls;

    const location = String(f.location.value).trim() || (this.fieldData ? this.fieldData.location : '');
    const name = String(f.name.value).trim() || (this.fieldData ? this.fieldData.name : '');
    const price = Number(f.price.value) ? Number(String(f.price.value).trim()) : (this.fieldData ? this.fieldData.price : 0);

    if (location?.length && name?.length && price) {
      const field: IField = {
        id: this.fieldData ? this.fieldData.id : Number(this.fieldService.getMaxId() + 1),
        location: location,
        name: name,
        price: price,
        options: true
      };
  
      this.formGroup.reset();
  
      this.fieldData ? this.fieldChange.emit(field) : this.fieldService.saveField(field);
    }
  }
}
