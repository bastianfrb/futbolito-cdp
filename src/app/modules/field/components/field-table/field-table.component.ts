import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { configurePaginator } from 'src/app/modules/shared/utils';
import { IField } from '../../interfaces/field';
import { FieldService } from '../../services/field.service';
import { ModalEditFieldComponent } from '../modal-edit-field/modal-edit-field.component';

@Component({
  selector: 'app-field-table',
  templateUrl: './field-table.component.html',
  styleUrls: ['./field-table.component.scss']
})
export class FieldTableComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  public dataSource: MatTableDataSource<IField>;

  public displayedColumns: string[];

  constructor(
    private fieldService: FieldService,
    public dialog: MatDialog, 
  ) {
    this.dataSource = new MatTableDataSource<IField>([]);
    this.displayedColumns = ['name', 'location', 'price', 'options'];
  }

  ngOnInit(): void {
    this.dataSource.data = this.fieldService.getFields();

    this.fieldService.fieldChanges.subscribe((fields: IField[]) => {
      this.dataSource.data = fields;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = configurePaginator(this.paginator);
  }

  /**
   * Function to edit field
   * @param id Field id
   */
  public editField(id: number): void {
    const field = this.fieldService.getFieldById(id);

    const dialogRef = this.dialog.open(ModalEditFieldComponent, {
      data: field
    });

    dialogRef.afterClosed().subscribe((fieldChange) => {
      this.fieldService.editField(fieldChange);
    });
  }

  /**
   * Function to remove field
   * @param id Field id
   */
  public removeField(id: number): void {
    this.fieldService.removeField(id);
  }

}
