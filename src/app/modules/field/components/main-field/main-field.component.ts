import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { CacheService } from 'src/app/modules/shared/services/cache/cache.service';
import { IField } from '../../interfaces/field';

@Component({
  selector: 'app-main-field',
  templateUrl: './main-field.component.html',
  styleUrls: ['./main-field.component.scss']
})
export class MainFieldComponent implements OnInit {
  

  constructor() {
  }

  ngOnInit(): void {
  }

}
