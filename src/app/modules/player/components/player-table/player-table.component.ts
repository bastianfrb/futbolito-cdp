import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { configurePaginator } from 'src/app/modules/shared/utils';
import { IPlayer } from '../../interfaces/player';
import { PlayerService } from '../../services/player.service';
import { ModalEditPlayerComponent } from '../modal-edit-player/modal-edit-player.component';

@Component({
  selector: 'app-player-table',
  templateUrl: './player-table.component.html',
  styleUrls: ['./player-table.component.scss']
})
export class PlayerTableComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  public dataSource: MatTableDataSource<IPlayer>;

  public displayedColumns: string[];

  constructor(
    private playerService: PlayerService,
    public dialog: MatDialog, 
  ) {
    this.dataSource = new MatTableDataSource<IPlayer>([]);
    this.displayedColumns = ['name', 'alias', 'position', 'options'];
  }

  ngOnInit(): void {
    this.dataSource.data = this.playerService.getPlayers();

    this.playerService.playerChanges.subscribe((players: IPlayer[]) => {
      this.dataSource.data = players;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = configurePaginator(this.paginator);
  }

  /**
   * Function to edit player
   * @param id Player id
   */
  public editPlayer(id: number): void {
    const player = this.playerService.getPlayerById(id);

    const dialogRef = this.dialog.open(ModalEditPlayerComponent, {
      data: player
    });

    dialogRef.afterClosed().subscribe((playerChange) => {
      this.playerService.editPlayer(playerChange);
    });
  }

  /**
   * Function to remove player
   * @param id Player id
   */
  public removePlayer(id: number): void {
    this.playerService.removePlayer(id);
  }

}
