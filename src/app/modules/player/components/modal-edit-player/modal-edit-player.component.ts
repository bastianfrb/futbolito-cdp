import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IPlayer } from '../../interfaces/player';

@Component({
  selector: 'app-modal-edit-player',
  templateUrl: './modal-edit-player.component.html',
  styleUrls: ['./modal-edit-player.component.scss']
})
export class ModalEditPlayerComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ModalEditPlayerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IPlayer,
  ) { }

  ngOnInit(): void {
  }

  public closeDialog(player: IPlayer): void {
    this.dialogRef.close(player ? player : this.data);
  }
}
