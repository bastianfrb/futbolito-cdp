import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IPlayer } from '../../interfaces/player';
import { PlayerService } from '../../services/player.service';

@Component({
  selector: 'app-player-form',
  templateUrl: './player-form.component.html',
  styleUrls: ['./player-form.component.scss']
})
export class PlayerFormComponent implements OnInit {

  @Input() public playerData?: IPlayer;

  @Output() public playerChange: EventEmitter<IPlayer>;

  public formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private playerService: PlayerService
  ) {
    this.playerChange = new EventEmitter<IPlayer>();

    this.formGroup = this.fb.group({
      name: [''],
      position: [''],
      alias: ['']
    });
  }

  ngOnInit(): void {
    if (this.playerData) {
      this.formGroup.patchValue({
        position: this.playerData.position,
        name: this.playerData.name,
        alias: this.playerData.alias
      });
    }
  }

  /**
   * Function to send form outside
   */
  public submitForm(): void {
    const f = this.formGroup.controls;

    const position = String(f.position.value).trim() || (this.playerData ? this.playerData.position : '');
    const name = String(f.name.value).trim() || (this.playerData ? this.playerData.name : '');
    const alias = String(f.alias.value).trim() || (this.playerData ? this.playerData.alias : '');

    if (position?.length && name?.length && alias?.length) {
      const player: IPlayer = {
        id: this.playerData ? this.playerData.id : Number(this.playerService.getMaxId() + 1),
        position,
        name,
        alias,
        pay: false,
        options: true
      };
  
      this.formGroup.reset();
  
      this.playerData ? this.playerChange.emit(player) : this.playerService.savePlayer(player);
    }
  }
}
