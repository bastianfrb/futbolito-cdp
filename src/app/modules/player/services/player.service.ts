import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CacheService } from '../../shared/services/cache/cache.service';
import { IPlayer } from '../interfaces/player';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  public playerChanges: Subject<IPlayer[]>;

  constructor(private cache: CacheService) { 
    this.playerChanges = new Subject<IPlayer[]>();
  }

  /**
   * Function to get maximun ID (provisory)
   * @returns max ID
   */
  public getMaxId(): number {
    const players = this.getPlayers();
    let maxId = 0;

    for (const player of players) {
      maxId = Number(player.id) > maxId ? player.id : maxId;
    }

    return maxId;
  }

  /**
   * Function to get available players
   */
  public getPlayers(): IPlayer[] {
    return this.cache.local('players') as IPlayer[] || [];
  }

  /**
   * Function to get a player by id
   * @id Player id
   */
  public getPlayerById(id: number): IPlayer {
    const player = this.getPlayers().find((v) => v.id === id) as IPlayer;
    return player;
  }
  
  /**
   * Function to save one new player
   * @player new player
   */
  public savePlayer(player: IPlayer): void {
    let players = this.cache.local('players') as IPlayer[];
    if (!players) {
      players = [];
    }

    players.push(player);

    this.cache.saveLocal('players', players);
    this.playerChanges.next(players);
  }
  
  /**
   * Function to edit a player
   * @param player Player to edit
   */
  public editPlayer(player: IPlayer): void {
    const players = this.getPlayers();
    const foundPlayer = players.find((v) => v.id === player.id) as IPlayer;

    foundPlayer.alias = player.alias;
    foundPlayer.name = player.name;
    foundPlayer.pay = player.pay;
    foundPlayer.position = player.position;

    this.cache.saveLocal('players', players);
    this.playerChanges.next(players);
  }

  /**
   * Function to remove a player
   * @param id Player id to remove
   */
  public removePlayer(id: number): void {
    const players = this.getPlayers().filter((v) => v.id !== id);

    this.cache.saveLocal('players', players);
    this.playerChanges.next(players);
  }
}
