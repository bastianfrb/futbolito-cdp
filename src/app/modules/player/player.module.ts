import { NgModule } from '@angular/core';
import { PlayerRoutingModule } from './player-routing.module';
import { CreatePlayerComponent } from './components/create-player/create-player.component';
import { PlayerFormComponent } from './components/player-form/player-form.component';
import { PlayerTableComponent } from './components/player-table/player-table.component';
import { MainPlayerComponent } from './components/main-player/main-player.component';
import { ModalEditPlayerComponent } from './components/modal-edit-player/modal-edit-player.component';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    CreatePlayerComponent,
    PlayerFormComponent,
    PlayerTableComponent,
    MainPlayerComponent,
    ModalEditPlayerComponent
  ],
  imports: [
    PlayerRoutingModule,
    MatCardModule,
    MatExpansionModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatDialogModule
  ],
})
export class PlayerModule { }
