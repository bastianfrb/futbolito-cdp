export interface IPlayer {
    id: number;
    name: string;
    position?: string;
    alias?: string;
    pay?: boolean;
    options: boolean;
}
