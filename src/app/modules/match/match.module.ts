import { NgModule } from '@angular/core';
import { MatchRoutingModule } from './match-routing.module';

@NgModule({
  declarations: [
  ],
  imports: [
    MatchRoutingModule,
  ],
})
export class MatchModule { }
