import { IField } from "../../field/interfaces/field";
import { IPlayer } from "../../player/interfaces/player";

export interface IMatch {
    date: string;
    durationMin: number;
    field: IField;
    players: IPlayer[];
}
