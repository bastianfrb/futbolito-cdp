import { Component } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent {

  public options = [
    { title: 'Canchas', redirect: 'field' },
    { title: 'Jugadores', redirect: 'player' },
    { title: 'Partidos', redirect: 'match' }
  ];

}
