import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/field',
    pathMatch: 'full',
  },
  {
    path: 'app',
    redirectTo: '/field',
    pathMatch: 'full'
  },
  {
    path: 'field',
    loadChildren: () => import('./modules/field/field.module').then((m) => m.FieldModule)
  },
  {
    path: 'player',
    loadChildren: () => import('./modules/player/player.module').then((m) => m.PlayerModule)
  },
  {
    path: '**',
    redirectTo: '/app'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
